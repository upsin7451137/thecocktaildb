
const cocktailsSection = document.getElementById('cocktails-section');
const totalAlcoholicSpan = document.getElementById('totalAlcoholic');
const totalNonAlcoholicSpan = document.getElementById('totalNonAlcoholic');
const totalAlcoholicText = document.getElementById('totalAlcoholicText');
const totalNonAlcoholicText = document.getElementById('totalNonAlcoholicText');

async function fetchCocktails() {
    const alcoholicRadio = document.getElementById('alcoholicRadio');
    const nonAlcoholicRadio = document.getElementById('nonAlcoholicRadio');

    const isAlcoholic = alcoholicRadio.checked;
    const isNonAlcoholic = nonAlcoholicRadio.checked;

    if (!isAlcoholic && !isNonAlcoholic) {
        alert('Selecciona al menos un tipo de coctel.');
        return;
    }

    const apiUrl = `https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=${isAlcoholic ? 'Alcoholic' : ''}${isNonAlcoholic ? 'Non_Alcoholic' : ''}`;

    try {
        const response = await fetch(apiUrl);
        const data = await response.json();

        displayCocktails(data.drinks);
    } catch (error) {
        console.error('Error fetching cocktails:', error);
    }
}

function displayCocktails(cocktails) {
    cocktailsSection.innerHTML = '';
    let totalAlcoholic = 0;
    let totalNonAlcoholic = 0;

    cocktails.forEach(cocktail => {
        const cocktailCard = document.createElement('div');
        cocktailCard.classList.add('cocktail-card');

        const cocktailImage = document.createElement('img');
        cocktailImage.src = cocktail.strDrinkThumb;
        cocktailImage.alt = cocktail.strDrink;

        const cocktailName = document.createElement('p');
        cocktailName.textContent = cocktail.strDrink;

        cocktailCard.appendChild(cocktailImage);
        cocktailCard.appendChild(cocktailName);

        cocktailsSection.appendChild(cocktailCard);

        if (cocktail.strAlcoholic === 'Alcoholic') {
            totalAlcoholic++;
        } else {
            totalNonAlcoholic++;
        }
    });

    totalAlcoholicSpan.textContent = totalAlcoholic;
    totalNonAlcoholicSpan.textContent = totalNonAlcoholic;

    // Mostrar u ocultar los totales según la búsqueda del usuario
    if (totalAlcoholic > 0 && totalNonAlcoholic > 0) {
        totalAlcoholicText.style.display = 'block';
        totalNonAlcoholicText.style.display = 'block';
    } else if (totalAlcoholic > 0) {
        totalAlcoholicText.style.display = 'block';
        totalNonAlcoholicText.style.display = 'none';
    } else if (totalNonAlcoholic > 0) {
        totalAlcoholicText.style.display = 'none';
        totalNonAlcoholicText.style.display = 'block';
    } else {
        totalAlcoholicText.style.display = 'none';
        totalNonAlcoholicText.style.display = 'none';
    }
}

function clearResults() {
    cocktailsSection.innerHTML = '';
    totalAlcoholicSpan.textContent = '0';
    totalNonAlcoholicSpan.textContent = '0';
    document.getElementById('alcoholicRadio').checked = false;
    document.getElementById('nonAlcoholicRadio').checked = false;

    // Ocultar los totales al limpiar los resultados
    totalAlcoholicText.style.display = 'none';
    totalNonAlcoholicText.style.display = 'none';
}
fetchCocktails();